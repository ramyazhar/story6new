from django import forms

class formKabar(forms.Form):
    Kabar = forms.CharField(widget = forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'kabar anda sedang gimana',
        'type' : 'text',
        'required' : True,
        'maxlength' : 300
    }))
