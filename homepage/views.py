from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def index(request):
    if(request.method == "POST"):
        tmp = forms.formKabar(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.coreDatabase()
            tmp2.kabarUser = tmp.cleaned_data["Kabar"]
            tmp2.save()
        return redirect("/")
    else:
        tmp = forms.formKabar()
        tmp2 = models.coreDatabase.objects.all()
        tmp_dictio = {
            'formKabar' : tmp,
            'coreDatabase' : tmp2
        }
        return render(request, 'homepage/homepage.html', tmp_dictio)
