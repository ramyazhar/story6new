from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from .views import index
from .models import coreDatabase
from .forms import formKabar
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/")
        self.assertEqual(found.func, index)

    def test_apakah_bisa_membuat_object(self):
        objectBaru2 = coreDatabase.objects.create(kabarUser = "TESTING")
        jumlahObjectYangAda = coreDatabase.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_kabar_di_post(self):
        test = 'Unknown'
        response_post = Client().post("/", {"Kabar" : test})
        today = timezone.now()
        kabar = coreDatabase.objects.first()
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(kabar.waktu.date(),today.date())
    
    def test_apakah_bisa_post_jika_kabar_blank(self):
        form = formKabar(data={"Kabar" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Kabar'], ['This field is required.'])

    def test_apakah_inputan_muncul_di_layar(self):
        response = Client().post('/', {'Kabar': "Testing"})
        today = timezone.now()
        new_response = Client().get("/")
        self.assertContains(new_response,"Testing")
    
    def test_apakah_tulisan_apa_kabar_muncul_dilayar(self):
        response = Client().get("/")
        self.assertContains(response, "Halo, apa kabar?")
    
    def test_apakah_form_muncul_dilayar(self):
        response = Client().get("/")
        self.assertContains(response, '<form')

    def test_apakah_tombol_masukan_muncul_dilayar(self):
        response = Client().get("/")
        self.assertContains(response, 'type="submit"')

class functionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome("./chromedriver" ,chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_websit(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn("Halo, apa kabar?", self.browser.page_source)
        time.sleep(3)
        self.browser.find_element_by_name('Kabar').send_keys("Coba Coba")
        time.sleep(3)
        self.browser.find_element_by_class_name('btn').click()
        time.sleep(3)
        self.assertIn("Coba Coba", self.browser.page_source)
