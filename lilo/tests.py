from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve, reverse
from lilo import views
from .views import SignIn

class AuthenticationTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('pewe', 'pewe@gmail.com', 'pewecool')
        cls.user.first_name = 'pewe'
        cls.user.save()
        
    def test_Authentication_url_exists(self):
        response = self.client.get(reverse('lilo:SignIn'))
        self.assertEqual(response.status_code, 200)

    def test_Authentication_using_Authentication_func(self):
        found = resolve(reverse('lilo:SignIn'))
        self.assertEqual(found.func, views.SignIn)

    def test_Authentication_using_about_template(self):
        response = self.client.get(reverse('lilo:SignIn'))
        self.assertTemplateUsed(response, 'SignIn.html')
        self.assertContains(response, 'username')
        self.assertContains(response, 'password')

    def test_SignIn_in_is_redirect(self):
        self.client.login(username='pewe', password='pewecool')
        response = self.client.get(reverse('lilo:SignIn'))
        self.assertEqual(response.status_code, 302)

    def test_SignIn_not_SignedIn(self):
        response = self.client.get(reverse('lilo:SignIn'))
        html = response.content.decode()
        self.assertIn('<form', html)

    def test_SignIn_submit(self):
        response = self.client.post(
            reverse('lilo:SignIn'), data={
                'username': 'pewe',
                'password' : 'pewecool',
            }
        )
        
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('books:books'))
        html = response.content.decode()
        self.assertIn(self.user.username, html)
    
    def test_SignIn_SignOut(self):
        response = self.client.post(
            reverse('lilo:SignIn'), data={
                'username': 'pewe',
                'password' : 'pewecool',
            }
        )

        self.client.get(reverse('lilo:SignOut'))
        response = self.client.get(reverse('books:books'))
        html = response.content.decode()
        self.assertNotIn(self.user.username, html)
