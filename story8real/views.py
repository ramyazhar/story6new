from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render
import json
import requests


def books(request):
    return render(request, 'ayax.html')

def search(request):
    url = 'https://www.googleapis.com/books/v1/volumes'
    url +=  f"?maxResults={10}&q={request.GET.get('q')}"
    data = requests.get(url).json()
    return JsonResponse(data, json_dumps_params={'indent': 2})
