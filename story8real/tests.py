from django.test import TestCase, Client
from django.urls import resolve, reverse
from story8real import views
from .views import books


# Create your tests here.
class Story_8_Unit_Test(TestCase):

    def test_story_8_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story_8_using_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, books)


    def test_story_8_using_books_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'ayax.html')

        
