from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest


## Create your tests here.
class UnitTest(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code,200)

    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)

    def test_apakah_tulisan_apa_kabar_muncul_dilayar(self):
        response = Client().get("/story7/")
        self.assertContains(response, "Hai, Apakabar? lagi")


class functionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(functionalTest,self).tearDown()

    # def test_halo(self):
    #     driver = self.selenium

    #     driver.get('http://127.0.0.1:8000/story7/')

    #     self.assertIn('Hai, Apakabar? lagi', driver.page_source)

    # def test_change_theme(self):
    #     driver = self.selenium
    #     driver.get('http://127.0.0.1:8000/story7/')

    #     body_background = driver.find_element_by_tag_name('body').value_of_css_property('background-color')
    #     self.assertIn('white', body_background)

    #     driver.find_element_by_tag_name('button').click()
    #     body_background = driver.find_element_by_name('body').value_of_css_property('background-color')
    #     self.assertIn('black', body_background)

    # def test_accordion_1(self):
    #     driver = self.selenium
    #     driver.get('http://127.0.0.1:8000/story7/')

    #     driver.find_element_by_id('accordion').click()
    #     self.assertIn('Dota',driver.page_source)